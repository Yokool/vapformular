package com.roj.comphelper;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;

public class CompAdd
{
    public static boolean container_contains_component(Container container, JComponent component)
    {
        return Arrays.stream(container.getComponents()).anyMatch((comp -> comp == component));
    }

    public static void component_safe_add(Container container, JComponent component, Object parameter)
    {
        if(container_contains_component(container, component))
        {
            return;
        }
        container.add(component, parameter);
        container.revalidate();
        container.repaint();
    }

    public static void component_safe_remove(Container container, JComponent component)
    {
        if(!container_contains_component(container, component))
        {
            return;
        }

        container.remove(component);
        container.revalidate();
        container.repaint();
    }
}

