package com.roj.customconstr;

import java.awt.*;

public class GBConstr
{
    private int grid_x = 0;
    private int grid_y = 0;
    private int fill;
    private double weight_x;
    private double weight_y;
    private int anchor = GridBagConstraints.CENTER;
    private int grid_width = 1;
    private int grid_height = 1;

    public GBConstr set_grid_width(int grid_width)
    {
        this.grid_width = grid_width;
        return this;
    }

    public GBConstr set_grid_weight(int grid_height)
    {
        this.grid_height = grid_height;
        return this;
    }

    public GBConstr()
    {
    }

    public GBConstr(GBConstr clone_obj)
    {
        set_grid_x(clone_obj.grid_x);
        set_grid_y(clone_obj.grid_y);
        set_fill(clone_obj.fill);
        set_weight_x(clone_obj.weight_x);
        set_weight_y(clone_obj.weight_y);
        set_anchor(clone_obj.anchor);
    }

    public GBConstr set_anchor(int anchor)
    {
        this.anchor = anchor;
        return this;
    }

    public GBConstr set_grid_x(int grid_x)
    {
        this.grid_x = grid_x;
        return this;
    }

    public GBConstr set_grid_y(int grid_y)
    {
        this.grid_y = grid_y;
        return this;
    }

    public GBConstr set_weight_x(double weight_x)
    {
        this.weight_x = weight_x;
        return this;
    }

    public GBConstr set_fill(int fill)
    {
        this.fill = fill;
        return this;
    }

    public GBConstr set_weight_y(double weight_y)
    {
        this.weight_y = weight_y;
        return this;
    }

    public GBConstr set_position(int grid_x, int grid_y)
    {
        set_grid_x(grid_x);
        return set_grid_y(grid_y);
    }

    public GridBagConstraints get_result()
    {
        GridBagConstraints result = new GridBagConstraints();
        result.gridx = grid_x;
        result.gridy = grid_y;
        result.fill = fill;
        result.weightx = weight_x;
        result.weighty = weight_y;
        result.anchor = anchor;
        result.gridwidth = grid_width;

        return result;
    }

}
