package com.roj.gender;

public class GenderConst
{

    public static final String MALE_LABEL = "Muž";
    public static final String FEMALE_LABEL = "Žena";
    public static final String OTHER_LABEL = "Jiné";

    public static final String MALE_ALIAS = "M";
    public static final String FEMALE_ALIAS = "F";
    public static final String OTHER_ALIAS = "O";


}
