package com.roj.components.panels;

import com.roj.compdata.ICompDataAsString;
import com.roj.comphelper.CompAdd;
import com.roj.customconstr.GBConstr;
import com.roj.gender.GenderConst;
import com.roj.listeners.ValidationChangeListener;
import com.roj.listeners.ValidationDocListener;
import com.roj.listeners.ValidationItemListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class GenderNewsPanel extends JPanel implements ICompDataAsString, IOnRegisterCompValidation {
    private JLabel gender_label = new JLabel("Pohlaví:");
    private JRadioButton male_button = new JRadioButton(GenderConst.MALE_LABEL);
    private JRadioButton female_button = new JRadioButton(GenderConst.FEMALE_LABEL);
    private JRadioButton other_button = new JRadioButton(GenderConst.OTHER_LABEL);

    private JLabel select_gender_warning = new JLabel("Prosím zvolte pohlaví.");

    private JCheckBox receive_newsletter = new JCheckBox("Dostávat novinky");
    private ButtonGroup gender_group = new ButtonGroup();

    private BirthdayNumberPanel tied_birthday_number_panel;

    private BirthdayNumberPanel get_tied_birthday_number_panel() {
        return this.tied_birthday_number_panel;
    }

    private void set_tied_birthday_number_panel(BirthdayNumberPanel tied_birthday_number_panel) {
        this.tied_birthday_number_panel = tied_birthday_number_panel;
    }

    private JLabel available_fields_label = new JLabel("Kraj:");
    private static final String[] available_fields = {
            "Hlavní město Praha",
            "Středočeský kraj",
            "Jihočeský kraj",
            "Plzeňský kraj",
            "Karlovarský kraj",
            "Ústecký kraj",
            "Liberecký kraj",
            "Královéhradecký kraj",
            "Pardubický kraj",
            "Kraj Vysočina",
            "Jihomoravský kraj",
            "Olomoucký kraj",
            "Moravskoslezský kraj",
            "Zlínský kraj"
    };

    private JComboBox<String> field_box = new JComboBox<>(available_fields);

    public GenderNewsPanel(BirthdayNumberPanel tied_birthday_number_panel) {
        set_tied_birthday_number_panel(tied_birthday_number_panel);
        comp_init();
    }

    private void comp_init() {
        select_gender_warning.setForeground(Color.red);
        setLayout(get_layout());
        add_children();
        ComponentValidation.run_validation(this);
    }

    private void setup_buttons() {
        gender_group.add(male_button);
        male_button.setActionCommand(GenderConst.MALE_ALIAS);
        gender_group.add(female_button);
        female_button.setActionCommand(GenderConst.FEMALE_ALIAS);
        gender_group.add(other_button);
        other_button.setActionCommand(GenderConst.OTHER_ALIAS);

        male_button.addItemListener(this::on_item_change);
        female_button.addItemListener(this::on_item_change);
        other_button.addItemListener(this::on_item_change);

    }

    public String get_selected_gender_str()
    {
        return gender_group.getSelection().getActionCommand();
    }

    public boolean is_gender_selected(String gender_str)
    {
        return get_selected_gender_str().equals(gender_str);
    }

    private void on_item_change(ItemEvent event)
    {
        ComponentValidation.run_validation(this);
        get_tied_birthday_number_panel().get_social_sec_number_panel().update_data_from_gender_selection(this);
    }

    private static GBConstr get_base_constr()
    {
        return new GBConstr()
                .set_fill(GridBagConstraints.BOTH);
    }

    private void add_children()
    {
        GBConstr base_constr = get_base_constr();

        setup_buttons();

        add(gender_label, new GBConstr(base_constr)
                .set_position(0, 0)
                .get_result()
        );

        add(male_button, new GBConstr(base_constr)
                .set_position(0, 1)
                .get_result()
        );

        add(female_button, new GBConstr(base_constr)
                .set_position(0, 2)
                .get_result()
        );

        add(other_button, new GBConstr(base_constr)
                .set_position(0, 3)
                .get_result()
        );

        add(receive_newsletter, new GBConstr(base_constr)
                .set_position(0, 5)
                .get_result()
        );

        add(available_fields_label, new GBConstr(base_constr)
                .set_position(0, 6)
                .get_result()
        );

        add(field_box, new GBConstr(base_constr)
                .set_position(0, 7)
                .get_result()
        );
    }

    private GridBagLayout get_layout()
    {
        GridBagLayout gb_layout = new GridBagLayout();

        gb_layout.columnWeights = new double[] {
                1.0
        };

        gb_layout.rowHeights = new int[] {
                20, // Label
                20, // Male
                20, // Female
                20, // Other
                20, // Gender warning label
                20, // Newsletter
                20, // Fields Label
                20  // Fields box
        };

        return gb_layout;
    }

    @Override
    public Object[] get_data()
    {
        ButtonModel gender_selection = gender_group.getSelection();

        if(gender_selection == null)
        {
            // We don't have enough data to give all the data
            // which is extracted from this component.
            return null;
        }

        return new Object[] {
                gender_selection.getActionCommand(),
                receive_newsletter.isSelected(),
                field_box.getSelectedItem(),
        };
    }

    @Override
    public boolean is_component_valid()
    {
        return gender_group.getSelection() != null;
    }

    @Override
    public void on_is_valid()
    {
        remove_gender_warning();
    }

    @Override
    public void on_is_invalid()
    {
        add_gender_warning();
    }

    private boolean is_showing_gender_warning()
    {
        return CompAdd.container_contains_component(this, select_gender_warning);
    }

    public void add_gender_warning()
    {
        if(is_showing_gender_warning())
        {
            return;
        }
        GBConstr base_constr = get_base_constr();

        add(select_gender_warning, new GBConstr(base_constr)
                .set_position(0, 4)
                .get_result()
        );

        revalidate();
        repaint();
    }

    public void remove_gender_warning()
    {
        if(!is_showing_gender_warning())
        {
            return;
        }

        remove(select_gender_warning);

        repaint();
        repaint();
    }
}
