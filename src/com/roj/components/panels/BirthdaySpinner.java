package com.roj.components.panels;

import com.roj.DateStr;
import com.roj.compdata.ICompDataAsString;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import java.util.Date;

public class BirthdaySpinner extends JSpinner implements ICompDataAsString
{

    private SocialSecNumberPanel tied_social_sec;

    private void set_social_sec(SocialSecNumberPanel tied_social_sec)
    {
        this.tied_social_sec = tied_social_sec;
    }

    public SocialSecNumberPanel get_social_sec()
    {
        return this.tied_social_sec;
    }

    public BirthdaySpinner(SocialSecNumberPanel tied_social_sec)
    {
        set_social_sec(tied_social_sec);
        comp_init();
        this.tied_social_sec.update_data_from_spinner(this);
    }

    private void comp_init()
    {
        SpinnerDateModel date_model = new SpinnerDateModel();

        setModel(date_model);
        setEditor(new JSpinner.DateEditor(this, "dd.MM.yyyy"));

        addChangeListener(this::spinner_update);
    }

    private void spinner_update(ChangeEvent change)
    {
        tied_social_sec.update_data_from_spinner(this);
    }

    @Override
    public Object[] get_data() {
        Date date = (Date) getValue();
        
        return new Object[] {
                DateStr.date_to_string(date)
        };
    }
}
