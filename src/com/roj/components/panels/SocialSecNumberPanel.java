package com.roj.components.panels;

import com.roj.compdata.ICompDataAsString;
import com.roj.comphelper.CompAdd;
import com.roj.customconstr.GBConstr;
import com.roj.gender.GenderConst;
import com.roj.interfaces.AddComponentFunc;

import javax.swing.*;
import java.awt.*;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class SocialSecNumberPanel extends JPanel implements ICompDataAsString, IOnRegisterCompValidation
{
    private JTextField number_year = new JTextField();
    private JTextField number_month = new JTextField();
    private JTextField number_day = new JTextField();

    private JLabel slash = new JLabel(" / ");
    private SocialSecInput number_unique = new SocialSecInput(4, this);

    private boolean add_female_fifty = false;

    private JLabel warning_label = new JLabel("Prosím vyplňte unikátní číslice rodného čísla");
    private ConditionalComponent warning_label_cond = new ConditionalComponent(this::add_warning_label, this::remove_warning_label);

    private void add_warning_label()
    {
        GBConstr base_constr = get_base_constr();
        CompAdd.component_safe_add(this, warning_label, new GBConstr(base_constr)
                .set_position(0, 1)
                .set_grid_width(8)
                .get_result()
        );
    }

    private void remove_warning_label()
    {
        CompAdd.component_safe_remove(this, warning_label);
    }

    private int year;
    private int month;
    private int day;

    public int get_year()
    {
        return this.year;
    }

    public void set_year(int year)
    {
        this.year = year;
        update_ui();
    }

    public void update_ui()
    {
        String year_last_two = String.valueOf(get_year()).substring(2, 4);
        number_year.setText(year_last_two);

        int month_number = get_month() + (add_female_fifty ? 50 : 0);
        number_month.setText(String.valueOf(month_number));

        number_day.setText(String.valueOf(get_day()));
    }

    public int get_month()
    {
        return this.month;
    }

    public void set_month(int month)
    {
        this.month = month;
        update_ui();
    }

    public int get_day()
    {
        return this.day;
    }

    public void set_day(int day)
    {
        this.day = day;
        update_ui();
    }

    public void update_data_from_gender_selection(GenderNewsPanel gender_news_panel)
    {
        this.add_female_fifty = gender_news_panel.is_gender_selected(GenderConst.FEMALE_ALIAS);
        update_ui();
    }

    public void update_data_from_spinner(JSpinner spinner)
    {
        Date d = (Date) spinner.getValue();
        set_data_from_date(d);
    }

    public void set_data_from_date(Date d)
    {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(d);

        int full_year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        set_data(full_year, month, day);
    }

    public void set_data(int full_year, int month, int day)
    {
        set_year(full_year);
        set_month(month);
        set_day(day);
    }

    public SocialSecNumberPanel()
    {
        comp_init();
    }

    private void comp_init()
    {
        number_year.setEditable(false);
        number_month.setEditable(false);
        number_day.setEditable(false);

        warning_label.setForeground(Color.red);

        setLayout(get_layout());
        add_children();
    }

    private GBConstr get_base_constr()
    {
        GBConstr base_contr = new GBConstr()
                .set_fill(GridBagConstraints.BOTH);
        return base_contr;
    }

    private void add_children()
    {
        GBConstr base_contr = get_base_constr();

        add(number_year, new GBConstr(base_contr)
                .set_position(0, 0)
                .get_result()
        );

        add(new JPanel(), new GBConstr(base_contr)
                .set_position(1, 0)
                .get_result()
        );

        add(number_month, new GBConstr(base_contr)
                .set_position(2, 0)
                .get_result()
        );

        add(new JPanel(), new GBConstr(base_contr)
                .set_position(3, 0)
                .get_result()
        );

        add(number_day, new GBConstr(base_contr)
                .set_position(4, 0)
                .get_result()
        );

        add(slash, new GBConstr(base_contr)
                .set_position(5, 0)
                .get_result()
        );

        add(number_unique, new GBConstr(base_contr)
                .set_position(6, 0)
                .get_result()
        );

        add(new JPanel(), new GBConstr(base_contr)
                .set_position(7, 0)
                .get_result()
        );

        add_warning_label();
    }

    private GridBagLayout get_layout()
    {
        GridBagLayout gb_layout = new GridBagLayout();

        final int LOW_NUMBER_WEIGHT = 20;
        final int GAP_WEIGHT = 1;
        final int UNIQUE_NUMBER_WEIGHT = 50;
        final int RIGHT_PAD_WEIGHT = 300;

        gb_layout.columnWeights = new double[] {
                LOW_NUMBER_WEIGHT, GAP_WEIGHT, LOW_NUMBER_WEIGHT, GAP_WEIGHT, LOW_NUMBER_WEIGHT, GAP_WEIGHT, UNIQUE_NUMBER_WEIGHT, RIGHT_PAD_WEIGHT
        };

        gb_layout.rowHeights = new int[] {
                20,
                20
        };

        return gb_layout;
    }

    @Override
    public Object[] get_data() {
        return new Object[] {
                number_year.getText(),
                number_month.getText(),
                number_day.getText(),
                number_unique.getText()
        };

    }

    @Override
    public void on_is_valid() {

    }

    @Override
    public void on_is_invalid() {

    }

    @Override
    public boolean is_component_valid() {
        boolean is_valid = number_unique.has_enough_digits();
        warning_label_cond.update_show_error(is_valid);
        return is_valid;
    }
}
