package com.roj.components.panels;

import com.roj.compdata.ICompDataAsString;
import com.roj.customconstr.GBConstr;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;

public class BirthdayNumberPanel extends JPanel implements ICompDataAsString, IOnRegisterCompValidation
{
    private JLabel birthdate_label = new JLabel("Datum narození");

    private JPanel birthday_slider_wrapper = new JPanel();
    private SocialSecNumberPanel social_sec_number_panel = new SocialSecNumberPanel();
    private BirthdaySpinner birthday_slider = new BirthdaySpinner(social_sec_number_panel);

    private JLabel social_sec_label = new JLabel("Rodné číslo");

    public SocialSecNumberPanel get_social_sec_number_panel()
    {
        return this.social_sec_number_panel;
    }

    public BirthdayNumberPanel()
    {
        comp_init();
    }

    private void comp_init()
    {
        setLayout(get_layout());
        add_children();
    }

    private GridBagLayout get_layout()
    {
        GridBagLayout layout = new GridBagLayout();

        layout.columnWeights = new double[] {
                1.0
        };

        layout.rowHeights = new int[] {
                20,
                20,
                20,
                40
        };

        return layout;
    }

    private GridBagLayout get_birthday_wrapper_layout()
    {
        GridBagLayout gb_layout = new GridBagLayout();

        gb_layout.columnWeights = new double[] {
             0.2, 0.8
        };

        return gb_layout;
    }

    private void add_children()
    {
        GBConstr base_constr = new GBConstr()
                .set_fill(GridBagConstraints.BOTH);

        add(birthdate_label, new GBConstr(base_constr)
                .set_position(0, 0)
                .get_result()
        );

        birthday_slider_wrapper.setLayout(get_birthday_wrapper_layout());

        birthday_slider_wrapper.add(birthday_slider, new GBConstr(base_constr)
                .set_position(0, 0)
                .get_result()
        );

        birthday_slider_wrapper.add(new JPanel(), new GBConstr(base_constr)
                .set_position(1, 0)
                .get_result()
        );

        add(birthday_slider_wrapper, new GBConstr(base_constr)
                .set_position(0, 1)
                .get_result()
        );

        add(social_sec_label, new GBConstr(base_constr)
                .set_position(0, 2)
                .get_result()
        );

        add(social_sec_number_panel, new GBConstr(base_constr)
                .set_position(0, 3)
                .get_result()
        );

    }

    @Override
    public Object[] get_data() {
        ArrayList<Object> data = new ArrayList<>();
        data.addAll(Arrays.stream(birthday_slider.get_data()).toList());
        data.addAll(Arrays.stream(social_sec_number_panel.get_data()).toList());

        return data.toArray();
    }

    @Override
    public void on_is_valid() {

    }

    @Override
    public void on_is_invalid() {

    }

    @Override
    public boolean is_component_valid() {
        return get_social_sec_number_panel().is_component_valid();
    }
}
