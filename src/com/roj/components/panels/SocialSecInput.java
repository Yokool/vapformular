package com.roj.components.panels;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.util.concurrent.locks.Condition;

public class SocialSecInput extends JTextField
{

    private int max_digits;

    private void set_allowed_digits(int max_digits)
    {
        this.max_digits = max_digits;
    }

    private int get_allowed_digits()
    {
        return this.max_digits;
    }

    public boolean has_enough_digits()
    {
        return getText().length() >= 3;
    }

    private IOnRegisterCompValidation validation_parent;

    private void set_validation_parent(IOnRegisterCompValidation validation_parent)
    {
        this.validation_parent = validation_parent;
    }

    private IOnRegisterCompValidation get_validation_parent()
    {
        return this.validation_parent;
    }

    private void run_parent_validation()
    {
        validation_parent.is_component_valid();
    }

    public SocialSecInput(int max_digits, IOnRegisterCompValidation validation_parent)
    {
        set_validation_parent(validation_parent);
        set_allowed_digits(max_digits);
        comp_init();
    }


    private void text_validate_input()
    {
        String orig_text = getText();
        StringBuilder text_builder = new StringBuilder(orig_text);
        text_builder = trim_from_non_num_chars(text_builder);

        if(text_builder.length() > get_allowed_digits())
        {
            text_builder = more_than_max_validation(text_builder);
        }

        String result = text_builder.toString();
        if(orig_text.equals(result))
        {
            return;
        }

        setText(result);
    }

    private StringBuilder more_than_max_validation(StringBuilder text_builder)
    {
        int last_index = text_builder.length() - 1;
        int pre_last_index = last_index - 1;

        char last_char = text_builder.charAt(last_index);

        text_builder.setCharAt(pre_last_index, last_char);
        text_builder.deleteCharAt(last_index);

        return text_builder;
    }
    private StringBuilder trim_from_non_num_chars(StringBuilder text_builder)
    {
        String str = text_builder.toString();
        String non_num_result = str.replaceAll("\\D", "");
        return new StringBuilder(non_num_result);
    }


    private void comp_init()
    {
        final Runnable remove_later = () -> {
            text_validate_input();
            run_parent_validation();
        };

        this.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                SwingUtilities.invokeLater(remove_later);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                SwingUtilities.invokeLater(remove_later);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                SwingUtilities.invokeLater(remove_later);
            }
        });
    }

}
