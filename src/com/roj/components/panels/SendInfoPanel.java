package com.roj.components.panels;

import com.roj.compdata.CompData;
import com.roj.compdata.ICompDataAsString;
import com.roj.components.panels.base.RegistrationPanel;
import com.roj.customconstr.GBConstr;
import com.roj.files.AppFiles;
import com.roj.separators.Separators;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class SendInfoPanel extends JPanel
{
    private RegistrationPanel registration_panel;
    private JButton register_button = new JButton("Registrovat se");

    public boolean is_registration_validation_success()
    {
        List<IOnRegisterCompValidation> validation_group = registration_panel.get_validation_group();
        return ComponentValidation.run_validation_group(validation_group);

    }

    public SendInfoPanel(RegistrationPanel registration_panel)
    {
        this.registration_panel = registration_panel;

        children_init();
        comp_init();
        add_children();
    }

    private void children_init()
    {
        register_button.addActionListener(this::on_register_button_click);
    }

    private void on_register_button_click(ActionEvent e)
    {
        if(!is_registration_validation_success())
        {
            JOptionPane.showMessageDialog(null, "Prosím vyřešte problémy.");
            return;
        }
        ICompDataAsString[] group = registration_panel.get_data_group();



        String data = CompData.comp_data_group_to_sep(group, Separators.COMMA);
        save_data_to_file(data);
        JOptionPane.showMessageDialog(null, "Registrace proběhla úspěšně.");
    }

    private void save_data_to_file(String data)
    {
        System.out.format("Přidávám data:\n%sdo %s\n", data, AppFiles.FORM_DATA_OUTPUT_FILE);
        AppFiles.add_data_to_output_file(data);
    }

    private GridBagLayout get_layout()
    {
        GridBagLayout gb_layout = new GridBagLayout();

        gb_layout.rowHeights = new int[] {
                30,
                60,
                30,
        };

        gb_layout.columnWeights = new double[] {
                0.2,
                0.6,
                0.2
        };

        return gb_layout;
    }

    private void comp_init()
    {
        setLayout(get_layout());
    }


    private void add_children()
    {
        GBConstr base_contr = new GBConstr()
                .set_fill(GridBagConstraints.BOTH);

        add(new JPanel(), new GBConstr(base_contr)
                .set_position(1, 0)
                .get_result()
        );
        add(new JPanel(), new GBConstr(base_contr)
                .set_position(1, 3)
                .get_result()
        );

        add(register_button, new GBConstr(base_contr)
                .set_position(1, 1)
            .get_result()
        );

        add(new JPanel(), new GBConstr(base_contr)
                .set_position(2, 1)
                .get_result()
        );
    }

}
