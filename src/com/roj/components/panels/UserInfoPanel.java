package com.roj.components.panels;

import com.roj.compdata.ICompDataAsString;
import com.roj.comphelper.CompAdd;
import com.roj.customconstr.GBConstr;
import com.roj.listeners.ValidationDocListener;
import com.roj.symbols.Symbols;

import javax.swing.*;
import java.awt.*;

public class UserInfoPanel extends JPanel implements ICompDataAsString, IOnRegisterCompValidation
{
    private static final String MISSING_NAME_WARN = "Prosím vyplňte jméno";
    private static final String MISSING_PASSWORD_WARN = "Prosím vyplňte heslo";

    private static final String ILLEGAL_NAME_WARN = "Jméno obsahuje nepovolené znaky.";
    private static final String ILLEGAL_PASSWORD_WARN = "Heslo obsahuje nepovolené znaky";


    private final JLabel username_label = new JLabel("Username:");
    private final JTextField username_field = new JTextField();

    private final JLabel username_warning_label = new JLabel(MISSING_NAME_WARN);
    private final ConditionalComponent username_warning_cond_component = new ConditionalComponent(this::add_username_warning, this::remove_username_warning);

    private final JLabel password_label = new JLabel("Password:");
    private final JPasswordField password_field = new JPasswordField();

    private final JLabel password_warning_label = new JLabel(MISSING_PASSWORD_WARN);
    private final ConditionalComponent password_warning_cond_component = new ConditionalComponent(this::add_password_warning, this::remove_password_warning);

    private void add_username_warning()
    {
        GBConstr base_constr = get_base_constr();
        CompAdd.component_safe_add(this, username_warning_label, new GBConstr(base_constr)
                .set_position(0, 2)
                .get_result()
        );

    }

    private void remove_username_warning()
    {
        CompAdd.component_safe_remove(this, username_warning_label);
    }

    private void add_password_warning()
    {
        GBConstr base_constr = get_base_constr();
        CompAdd.component_safe_add(this, password_warning_label, new GBConstr(base_constr)
                .set_position(0, 5)
                .get_result()
        );
    }

    private void remove_password_warning()
    {
        CompAdd.component_safe_remove(this, password_warning_label);
    }



    public UserInfoPanel()
    {
        children_init();
        comp_init();
    }

    private void children_init()
    {
        ValidationDocListener validation_listener = new ValidationDocListener(this);
        username_field.getDocument().addDocumentListener(validation_listener);
        password_field.getDocument().addDocumentListener(validation_listener);
    }


    private static GBConstr get_base_constr()
    {
        return new GBConstr()
            .set_fill(GridBagConstraints.BOTH);
    }

    private void comp_init()
    {
        username_warning_label.setForeground(Color.red);
        password_warning_label.setForeground(Color.red);

        setLayout(get_layout());

        GBConstr base_contr = get_base_constr();

        add(username_label, new GBConstr(base_contr)
                .set_position(0, 0)
                .get_result()
        );

        add(username_field, new GBConstr(base_contr)
                .set_position(0, 1)
                .get_result()
        );

        add(password_label, new GBConstr(base_contr)
                .set_position(0, 3)
                .get_result()
        );

        add(password_field, new GBConstr(base_contr)
                .set_position(0, 4)
                .get_result()
        );

        is_component_valid();
    }

    private GridBagLayout get_layout()
    {
        GridBagLayout gb_layout = new GridBagLayout();
        gb_layout.rowHeights = new int[] {
                30, // Username label
                30, // Username field
                30, // Username error
                30, // Password label
                30, // Password field
                30, // Password warning
        };

        gb_layout.columnWeights = new double[] {
                1.0
        };


        return gb_layout;
    }

    @Override
    public Object[] get_data() {
        String user_name = username_field.getText();
        char[] password = password_field.getPassword();

        return new Object[] {
            user_name,
            String.valueOf(password)
        };

    }

    private boolean is_username_filled()
    {
        return username_field.getText().length() != 0;
    }

    private boolean is_password_filled()
    {
        return password_field.getPassword().length != 0;
    }

    private boolean is_username_legal()
    {
        return Symbols.is_string_legal(username_field.getText());
    }

    private boolean is_password_legal()
    {
        String password = new String(password_field.getPassword());
        return Symbols.is_string_legal(password);
    }

    @Override
    public boolean is_component_valid()
    {
        String username_msg = "";
        boolean is_username_filled = is_username_filled();
        username_msg = !is_username_filled ? MISSING_NAME_WARN : username_msg;
        boolean is_username_legal = is_username_legal();
        username_msg = !is_username_legal ? ILLEGAL_NAME_WARN : username_msg;

        username_warning_label.setText(username_msg);
        username_warning_cond_component.update_show_error(is_username_filled && is_username_legal);


        String password_msg = "";

        boolean is_password_filled = is_password_filled();
        password_msg = !is_password_filled ? MISSING_PASSWORD_WARN : password_msg;

        boolean is_password_legal = is_password_legal();
        password_msg = !is_password_legal ? ILLEGAL_PASSWORD_WARN : password_msg;

        password_warning_label.setText(password_msg);
        password_warning_cond_component.update_show_error(is_password_filled && is_password_legal);


        return is_username_filled && is_username_legal && is_password_filled && is_password_legal;
    }

    @Override
    public void on_is_valid()
    {

    }

    @Override
    public void on_is_invalid()
    {

    }
}
