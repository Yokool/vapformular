package com.roj.listeners;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class ValidationChangeListener extends ValidationListener implements ChangeListener
{
    public ValidationChangeListener(IValidationFunc validation_func)
    {
        super(validation_func);
    }


    @Override
    public void stateChanged(ChangeEvent e) {
        perform_validation();
    }
}
