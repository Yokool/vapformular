package com.roj.compdata;

public class CompData
{

    public static String comp_data_group_to_sep(ICompDataAsString[] group, String separator)
    {
        StringBuilder sep_string = new StringBuilder();

        for (int i = 0; i < group.length; ++i) {
            ICompDataAsString comp_data_el = group[i];
            Object[] data = comp_data_el.get_data();
            boolean last_of_group = i == group.length - 1;

            for(int j = 0; j < data.length; ++j)
            {
                boolean last_of_data_group = j == data.length - 1;

                Object data_element = data[j];
                sep_string.append(data_element);


                if(!(last_of_group && last_of_data_group))
                {
                    sep_string.append(separator);
                }
                else
                {
                    sep_string.append("\n");
                }

            }
        }

        return sep_string.toString();
    }

}
