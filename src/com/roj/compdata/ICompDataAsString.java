package com.roj.compdata;

@FunctionalInterface
public interface ICompDataAsString
{
    Object[] get_data();
}
