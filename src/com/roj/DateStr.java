package com.roj;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateStr
{

    public static String date_to_string(Date date)
    {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);

        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH) + 1;
        int year = calendar.get(Calendar.YEAR);

        return String.format("%d.%d.%d", day, month, year);
    }

}
